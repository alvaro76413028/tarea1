package com.venta.controller;

import com.venta.exception.ModeloNotFoundException;
import com.venta.model.Persona;
import com.venta.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personas")
public class PersonaController {

    @Autowired
    private IPersonaService service;

    @PostMapping
    public ResponseEntity<Persona> registrar(@RequestBody Persona persona){
        Persona p= service.registrar(persona);

        return new ResponseEntity<Persona>(p, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Persona> modificar(@RequestBody Persona persona){
        Persona p= service.modificar(persona);

        return new ResponseEntity<Persona>(p, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Persona> leer(@PathVariable("id") Integer idP){
        Persona p= service.leer(idP);
        if(p == null){
            throw new ModeloNotFoundException("ID no encontrado: " + idP);
        }

        return new ResponseEntity<Persona>(p, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Persona>> listar(){
        List<Persona> lista=  service.listar();

        return new ResponseEntity<List<Persona>>(lista,HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
        Persona p= service.leer(id);
        if(p==null){
            throw new ModeloNotFoundException("ID no encontrado: " + id);
        }
        else {
            service.eliminar(id);
        }

        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
