package com.venta.controller;

import com.venta.exception.ModeloNotFoundException;
import com.venta.model.Producto;
import com.venta.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    private IProductoService service;

    @PostMapping
    public ResponseEntity<Producto> registrar(@RequestBody Producto producto){
        Producto p= service.registrar(producto);

        return new ResponseEntity(p, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Producto> modificar(@RequestBody Producto producto){
        Producto p= service.modificar(producto);

        return new ResponseEntity(p, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Producto> leer(@PathVariable("id") Integer id){
        Producto p= service.leer(id);

        if(p == null){
            throw new ModeloNotFoundException("ID no encontrado: " + id);
        }

        return new ResponseEntity(p, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Producto>> listar(){
        List<Producto> lista= service.listar();

        return new ResponseEntity(lista,HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
        Producto p= service.leer(id);

        if(p==null){
            throw new ModeloNotFoundException("ID no encontrado: " + id);
        }
        else{
            service.eliminar(id);
        }

        return new ResponseEntity(p, HttpStatus.OK);
    }
}
