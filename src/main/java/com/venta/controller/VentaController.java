package com.venta.controller;

import com.venta.model.Venta;
import com.venta.service.IVentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ventas")
public class VentaController {

    @Autowired
    private IVentaService service;


    @PostMapping
    public ResponseEntity<Venta> registrar(@RequestBody Venta venta){
        Venta v= service.registrar(venta);

        return new ResponseEntity(v, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Venta>> listar(){
        List<Venta> lista= service.listar();

        return new ResponseEntity(lista, HttpStatus.OK);
    }
}
