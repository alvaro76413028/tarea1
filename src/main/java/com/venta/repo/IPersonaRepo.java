package com.venta.repo;

import com.venta.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
