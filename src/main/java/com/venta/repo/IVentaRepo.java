package com.venta.repo;

import com.venta.model.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IVentaRepo extends JpaRepository<Venta, Integer> {

}
