package com.venta.service.impl;

import com.venta.model.Persona;
import com.venta.repo.IPersonaRepo;
import com.venta.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private IPersonaRepo repo;

    @Override
    public Persona registrar(Persona persona) {
        return repo.save(persona);
    }

    @Override
    public Persona modificar(Persona persona) {
        return repo.save(persona);
    }

    @Override
    public Persona leer(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    public List<Persona> listar() {
        return repo.findAll();
    }

    @Override
    public void eliminar(Integer id) {
         repo.deleteById(id);
    }
}
