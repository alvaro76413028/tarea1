package com.venta.service.impl;

import com.venta.model.Producto;
import com.venta.repo.IProductoRepo;
import com.venta.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductoServiceImpl implements IProductoService {

    @Autowired
    private IProductoRepo repo;


    @Override
    public Producto registrar(Producto producto) {
        return repo.save(producto);
    }

    @Override
    public Producto modificar(Producto producto) {
        return repo.save(producto);
    }

    @Override
    public Producto leer(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    public List<Producto> listar() {
        return repo.findAll();
    }

    @Override
    public void eliminar(Integer id) {
          repo.deleteById(id);
    }
}
