package com.venta.service.impl;

import com.venta.model.DetalleVenta;
import com.venta.model.Venta;
import com.venta.repo.IVentaRepo;
import com.venta.service.IVentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VentaServiceImpl implements IVentaService {

    @Autowired
    private IVentaRepo repo;


    @Override
    public Venta registrar(Venta venta) {

        for(DetalleVenta det : venta.getDetalleVenta()){
            det.setVenta(venta);
        }

        return repo.save(venta);
    }

    @Override
    public Venta modificar(Venta venta) {
        return null;
    }

    @Override
    public Venta leer(Integer id) {
        return null;
    }

    @Override
    public List<Venta> listar() {
        return repo.findAll();
    }

    @Override
    public void eliminar(Integer id) {

    }
}
